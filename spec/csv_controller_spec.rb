# frozen_string_literal: true

require 'spec_helper'
require 'csv'
require './csv_controller'

describe 'csv_controller' do
  let(:ctrl) { CsvController.new }

  describe '#new_rows' do
    let(:headers) { [:key, :ja, :en] }
    let(:expect_new_rows) {[
      ['key.new_row', '新規行', 'new_row'],
      ['key.new_row2', '新規行2', 'new_row2'],
    ]}

    context '新規行が抽出できているかどうか' do
      it { expect(ctrl.new_rows).to eq csv_rows(headers, expect_new_rows) }
    end
  end

  describe '#insert' do
    let(:headers) { [:key, :ja, :zh_cz, :zh_tw] }
    let(:expect_rows) {[
      [:key, :ja, :zh_cz, :zh_tw],
      %w(key.test 試験 测试 測試),
      %w(key.name 名前 氏 氏),
      %w(key.deleted_row 削除済み行),
      %w(key.deleted_row2 削除済み行2),
      %w(key.edited_row 編集_前),
      %w(key.edited_row2 編集_前2),
      ['key.new_row', '新規行', '', ''],
      ['key.new_row2', '新規行2', '', ''],
    ]}

    context '新規行が挿入できているかどうか' do
      it { expect([*ctrl.insert]).to eq csv_rows(headers, expect_rows).map(&:fields) }
    end
  end

  describe '#deleted_rows' do
    let(:headers) { [:key, :ja, :zh_cz, :zh_tw] }
    let(:expect_deleted_rows) {[
      ['key.deleted_row', '削除済み行', nil, nil],
      ['key.deleted_row2', '削除済み行2', nil, nil],
    ]}

    context '削除行が抽出できているかどうか' do
      it { expect(ctrl.deleted_rows).to eq csv_rows(headers, expect_deleted_rows) }
    end
  end

  describe '#edited_rows' do
    let(:headers) { [:key, :ja, :zh_cz, :zh_tw] }
    let(:expect_deleted_rows) {[
      ['key.edited_row', '編集_前', nil, nil],
      ['key.edited_row2', '編集_前2', nil, nil],
    ]}

    context '削除行が抽出できているかどうか' do
      it { expect(ctrl.edited_rows).to eq csv_rows(headers, expect_deleted_rows) }
    end
  end

  def csv_rows(headers, fields)
    fields.map { |row| CSV::Row.new(headers, row) }
  end
end
