# frozen_string_literal: true

require 'csv'
require 'hashie'
require 'pry-byebug'

config = Hashie::Mash.load('config.yml')

EN_PATH = "#{config.root}/mock_data/en.csv"
ZH_PATH = "#{config.root}/mock_data/zh.csv"

class CsvController
  def initialize
    @en_csv = CSV.table(EN_PATH, headers:true, encoding: 'UTF-8')
    @zh_csv = CSV.table(ZH_PATH, headers:true, encoding: 'UTF-8')
  end

  def new_rows
    @en_csv.reject { |en_row| @zh_csv[:key].include?(en_row[:key]) }
  end

  # TODO 末尾挿入でなくen.csvと同じ行目に合わせる必要がある
  # 新規行をzh.csvに追加する
  # return [[headers], *[values]]
  def insert
    insert_rows = new_rows.map do |row|
      [row[:key], row[:ja], '', '']
    end
    @zh_csv.push(*insert_rows) # 最終行に追加
  end

  def deleted_rows
    @zh_csv.reject { |zh_row| @en_csv[:key].include?(zh_row[:key]) }
  end

  def edited_rows
    rows = []
    @en_csv.each do |en_row|
      @zh_csv.each do |zh_row|
        rows << zh_row if edited_row?(en_row, zh_row)
      end
    end
    rows
  end

  # keyが同じで日本語が違う行は編集された行とみなす
  def edited_row?(row1, row2)
    row1[:key] == row2[:key] && row1[:ja] != row2[:ja]
  end
end
